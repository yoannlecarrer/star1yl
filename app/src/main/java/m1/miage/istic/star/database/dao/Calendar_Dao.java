package m1.miage.istic.star.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import m1.miage.istic.star.models.Calendar;

@Dao
public interface Calendar_Dao {

    @Query("SELECT * FROM Calendar WHERE service_id = :service_id")
    LiveData<List<Calendar>> getCalendar(long service_id);

    @Insert
    long insertCalendar(Calendar calendar);

    @Update
    int updateCalendar(Calendar calendar);

    @Query("DELETE FROM Calendar WHERE service_id = :service_id")
    int deleteCalendar(long service_id);

}