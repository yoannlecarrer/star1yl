package m1.miage.istic.star;

import android.app.DownloadManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;


public class MyService extends Service {


    private static final String CHANNEL_ID = "channelID";
    DownloadManager downloadManager;
    String[][] tabCalendar = new String[50][10];
    String[][] tabBus = new String[50][8];
    String[][] tabStop = new String[50][7];
    String[][] tabStop_times = new String[50][6];
    String[][] tabTrip = new String[50][7];
    final String SEPARATEUR = ",";
    String result;
    String dateDebut = "";
    String dateFin = "";
    String url =null;

    @Override
    public void onCreate() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Toast.makeText(getApplicationContext(), "Service started", Toast.LENGTH_LONG).show();
        //downloadJSONFile();
        try {
            readCalendar();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Thread.sleep(3000);
            readFile();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return START_NOT_STICKY;
    }


    public void readFile() throws IOException {
        int i = 0;
        String url = null;
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/fichier.json");

        FileInputStream stream = new FileInputStream(file);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line = "";
        while (line != null) {
            line = bufferedReader.readLine();
            result = result + line;
        }
        bufferedReader.close();
        String data = result.substring(4);

        try {
            org.json.JSONArray ja = new org.json.JSONArray(data);
            for (i = 0; i < ja.length(); i++) {
                JSONObject jo = (JSONObject) ja.get(i);
                JSONObject joDate = (JSONObject) jo.getJSONObject("fields");
                dateDebut = (String) joDate.getString("debutvalidite");
                dateFin = (String) joDate.getString("finvalidite");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date dateDebutParsed = sdf.parse(dateDebut);
                Date dateFinParsed = sdf.parse(dateFin);
                if (new Date().before(dateDebutParsed)) {
                    Toast.makeText(getApplicationContext(), "la date " + (i + 1) + " ne correspond pas", Toast.LENGTH_LONG).show();
                } else if (new Date().after(dateDebutParsed) && new Date().before(dateFinParsed)) {
                    Toast.makeText(getApplicationContext(), "la date " + (i + 1) + " correspond", Toast.LENGTH_LONG).show();
                    url = joDate.getString("url");
                    break;
                }
            }

        } catch (JSONException | ParseException e) {
            e.printStackTrace();
        }
        this.url=url;
        if(url != null){
            createNotification();
        }
    }

    public void downloadJSONFile() {
        downloadManager = (DownloadManager) getSystemService(getApplicationContext().DOWNLOAD_SERVICE);
        Uri uri = Uri.parse("https://data.explore.star.fr/explore/dataset/tco-busmetro-horaires-gtfs-versions-td/download/?format=json&timezone=Europe/Berlin&lang=fr");
        DownloadManager.Request request = new DownloadManager.Request(uri);
        //request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "fichier.json");
        request.setTitle("Téléchargement du fichier JSON ...");
        Long reference = downloadManager.enqueue(request);

            downloadManager.getUriForDownloadedFile(reference);

        File filepath = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/fichier.json");
        Toast.makeText(getApplicationContext(), "fichier JSON téléchargé avec succès dans:\n" + filepath, Toast.LENGTH_LONG).show();
    }

    public void createNotification() throws IOException {

        final int progressMax = 100;

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Intent activityIntent = new Intent(MyService.this, MyActivity.class);
        activityIntent.putExtra("Url file", getUrl());

        PendingIntent pendingIntent = PendingIntent.getActivity(MyService.this,0,activityIntent,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notification = new NotificationCompat.Builder(MyService.this, CHANNEL_ID)
                .setContentTitle("Horraire Bus STAR")
                .setContentText("Nouveau fichier disponible")
                .setSmallIcon(R.drawable.ic_notification)
                .setColor(Color.BLUE)
                .setContentIntent(pendingIntent)
                //.setPriority()
                .setAutoCancel(true)
                .addAction(R.mipmap.ic_launcher, "Télécharger", pendingIntent);
        notificationManager.notify(1, notification.build());
        
/*
        new Thread(new Runnable() {
            @Override
            public void run() {
                SystemClock.sleep(2000);
                for (int progress =0; progress <= progressMax; progress+=10){
                    notification.setProgress(progressMax, progress,false);
                    notificationManager.notify(2,notification.build());
                    SystemClock.sleep(1000);

                }
                notification.setContentText("Download finished")
                .setProgress(0,0,false)
                .setOngoing(false);
                notificationManager.notify(2,notification.build());

            }
        }).start();
 */
    }

    public String getUrl()
    {
        return url;
    }

    private String[][] readCalendar() throws IOException {
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/calendar.txt");
            FileInputStream stream = new FileInputStream(file);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            int j = 0;
            String ligne;
            while(bufferedReader.ready())
            {
                ligne = bufferedReader.readLine();
                String mots[] = ligne.split(SEPARATEUR);
                for (int i = 0; i < mots.length; i++) {
                    if(i==0){
                        Log.v("readCalendar","mots : "+mots[i]);
                        tabCalendar[j][i] = mots[i];
                    }
                    if(i==1){
                        tabCalendar[j][i] = mots[i];
                    }
                    if(i==2){
                        tabCalendar[j][i] = mots[i];
                    }
                    if(i==3){
                        tabCalendar[j][i] = mots[i];
                    }
                    if(i==4){
                        tabCalendar[j][i] = mots[i];
                    }
                    if(i==5){
                        tabCalendar[j][i] = mots[i];
                    }
                    if(i==6){
                        tabCalendar[j][i] = mots[i];
                    }
                    if(i==7){
                        tabCalendar[j][i] = mots[i];
                    }
                    if(i==8){
                        tabCalendar[j][i] = mots[i];
                    }
                    if(i==9){
                        tabCalendar[j][i] = mots[i];
                    }
                }
                j++;
                Log.v("readCalendar","ligne : "+ligne);
                Log.v("readCalendar",""+Arrays.deepToString(tabCalendar));
            }
            bufferedReader.close();

        } catch (Exception  e) {
            e.printStackTrace();
        }
        Log.v("tab"," "+tabCalendar);
        return tabCalendar;
    }

    private String[][] readBus() throws IOException {
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/routes.txt");
            FileInputStream stream = new FileInputStream(file);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            String ligne;
            int j = 0;
            while(bufferedReader.ready())
            {
                ligne = bufferedReader.readLine();
                String mots[] = ligne.split(SEPARATEUR);
                for (int i = 0; i < mots.length; i++) {
                    if(i==0){
                        tabBus[j][i] = mots[i];
                    }
                    if(i==2){
                        tabBus[j][1] = mots[i];
                    }
                    if(i==3){
                        tabBus[j][2] = mots[i];
                    }
                    if(i==4){
                        tabBus[j][3] = mots[i];
                    }
                    if(i==5){
                        tabBus[j][4] = mots[i];
                    }
                    if(i==7){
                        tabBus[j][5] = mots[i];
                    }
                    if(i==8){
                        tabBus[j][6] = mots[i];
                    }
                }
                j++;
                Log.v("readBus","ligne : "+ligne);
                Log.v("readBus",""+Arrays.deepToString(tabBus));

            }
            bufferedReader.close();

        } catch (Exception  e) {
            e.printStackTrace();
        }
        return tabBus;
    }

    private String[][] readStop() throws IOException {
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/stops.txt");
            FileInputStream stream = new FileInputStream(file);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            String ligne;
            int j = 0;
            while(bufferedReader.ready())
            {
                ligne = bufferedReader.readLine();
                String mots[] = ligne.split(SEPARATEUR);
                for (int i = 0; i < mots.length; i++) {
                    if(i==1){
                        tabStop[j][0] = mots[i];
                    }
                    if(i==2){
                        tabStop[j][1] = mots[i];
                    }
                    if(i==3){
                        tabStop[j][2] = mots[i];
                    }
                    if(i==4){
                        tabStop[j][3] = mots[i];
                    }
                    if(i==5){
                        tabStop[j][4] = mots[i];
                    }
                    if(i==11){
                        tabStop[j][5] = mots[i];
                    }
                }
                j++;
                Log.v("readStop","ligne : "+ligne);
                Log.v("readStop",""+Arrays.deepToString(tabStop));
            }
            bufferedReader.close();

        } catch (Exception  e) {
            e.printStackTrace();
        }
        return tabStop;
    }

    private String[][] readStopTime() throws IOException {
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/stop_times.txt");
            FileInputStream stream = new FileInputStream(file);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            String ligne;
            int j = 0;
            while(bufferedReader.ready())
            {
                ligne = bufferedReader.readLine();
                String mots[] = ligne.split(SEPARATEUR);
                for (int i = 0; i < mots.length; i++) {
                    if(i==0){
                        tabStop_times[j][i] = mots[i];
                    }
                    if(i==1){
                        tabStop_times[j][i] = mots[i];
                    }
                    if(i==2){
                        tabStop_times[j][i] = mots[i];
                    }
                    if(i==3){
                        tabStop_times[j][i] = mots[i];
                    }
                    if(i==4){
                        tabStop_times[j][i] = mots[i];
                    }
                }
                j++;
                Log.v("readStopTime","ligne : "+ligne);
                Log.v("readStopTime",""+Arrays.deepToString(tabStop_times));
                //Log.v("geg",""+Arrays.deepToString(tabTrip));
                //Log.v("Test","ligne : "+ligne);
                //Log.v("stop_id"," : "+stop_id);
                //Log.v("trip_id"," : "+trip_id);
                //Log.v("arrival_time"," : "+arrival_time);
                //Log.v("departure_time"," : "+departure_time);
                //.v("stop_sequence"," : "+stop_sequence);
            }
            bufferedReader.close();

        } catch (Exception  e) {
            e.printStackTrace();
        }
        return tabStop_times;
}

    private String[][] readTrip() throws IOException {
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/trips.txt");
            FileInputStream stream = new FileInputStream(file);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            String ligne;
            int j = 0;
            while(bufferedReader.ready())
            {
                ligne = bufferedReader.readLine();
                String mots[] = ligne.split(SEPARATEUR);
                for (int i = 0; i < mots.length; i++) {
                    if(i==0){
                        tabTrip[j][i] = mots[i];
                    }
                    if(i==1){
                        tabTrip[j][i] = mots[i];
                    }
                    if(i==2){
                        tabTrip[j][i] = mots[i];
                    }
                    if(i==3){
                        tabTrip[j][i] = mots[i];
                    }
                    if(i==5){
                        tabTrip[j][4] = mots[i];
                    }
                    if(i==6){
                        tabTrip[j][5] = mots[i];
                    }
                    if(i==8){
                        tabTrip[j][6] = mots[i];
                    }
                }
                j++;
                Log.v("readTrip","ligne : "+ligne);
                Log.v("readTrip",""+Arrays.deepToString(tabTrip));
                //Log.v("Test","ligne : "+ligne);
                //Log.v("trip_id"," : "+trip_id);
                //Log.v("route_id"," : "+route_id);
                //Log.v("service_id"," : "+service_id);
                //Log.v("trip_headsign"," : "+trip_headsign);
                //Log.v("direction_id"," : "+direction_id);
                //Log.v("block_id"," : "+block_id);
                //Log.v("wheelchair_accessible"," : "+wheelchair_accessible);
            }
            Log.v("jyjyjyjyjyjyj",""+ Arrays.deepToString(tabTrip));
            bufferedReader.close();

        } catch (Exception  e) {
            e.printStackTrace();
        }
        return tabTrip;
    }
}
