package m1.miage.istic.star.contenu;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import java.util.HashMap;

import m1.miage.istic.star.database.SaveMyDatabase;
import m1.miage.istic.star.database.dao.Bus_Route_Dao;
import m1.miage.istic.star.database.dao.Calendar_Dao;
import m1.miage.istic.star.database.dao.Stop_Dao;
import m1.miage.istic.star.database.dao.Stop_Time_Dao;
import m1.miage.istic.star.database.dao.Trip_Dao;
import m1.miage.istic.star.models.Bus_Route;

public class StarProvider extends ContentProvider {

    private Bus_Route_Dao busRouteDao;
    private Calendar_Dao calendarDao;
    private Stop_Dao stopDao;
    private Stop_Time_Dao stopTimeDao;
    private Trip_Dao tripDao;

    private static final int DIR = 0;
    private static final int ITEM = 1;

    private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
    static{
        URI_MATCHER.addURI(StarContract.CONTENT_AUTHORITY, StarContract.PATH_BUSROUTE, DIR);
        URI_MATCHER.addURI(StarContract.CONTENT_AUTHORITY, StarContract.PATH_BUSROUTE + "/#", ITEM);

        URI_MATCHER.addURI(StarContract.CONTENT_AUTHORITY, StarContract.PATH_CALENDAR, DIR);
        URI_MATCHER.addURI(StarContract.CONTENT_AUTHORITY, StarContract.PATH_CALENDAR + "/#", ITEM);

        URI_MATCHER.addURI(StarContract.CONTENT_AUTHORITY, StarContract.PATH_STOPS, DIR);
        URI_MATCHER.addURI(StarContract.CONTENT_AUTHORITY, StarContract.PATH_STOPS + "/#", ITEM);

        URI_MATCHER.addURI(StarContract.CONTENT_AUTHORITY, StarContract.PATH_STOPSTIME, DIR);
        URI_MATCHER.addURI(StarContract.CONTENT_AUTHORITY, StarContract.PATH_STOPSTIME + "/#", ITEM);

        URI_MATCHER.addURI(StarContract.CONTENT_AUTHORITY, StarContract.PATH_TRIP, DIR);
        URI_MATCHER.addURI(StarContract.CONTENT_AUTHORITY, StarContract.PATH_TRIP + "/#", ITEM);
    }


    @Override
    public boolean onCreate() {
        return true;
    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (getContext() != null){
            long route_id = ContentUris.parseId(uri);
            final Cursor cursor = SaveMyDatabase.getInstance(getContext()).bus_route_dao().getItemsWithCursor(route_id);
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
            return cursor;
        }

        throw new IllegalArgumentException("Failed to query row for uri " + uri);
    }

    @Override
    public String getType(Uri uri) {
        return "vnd.android.cursor.item/" + StarContract.CONTENT_AUTHORITY + "." + "bus_route";
        }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        if (getContext() != null){
            final long id = SaveMyDatabase.getInstance(getContext()).bus_route_dao().insertBusRoute(Bus_Route.fromContentValues(values));
            if (id != 0){
                getContext().getContentResolver().notifyChange(uri, null);
                return ContentUris.withAppendedId(uri, id);
            }
        }

        throw new IllegalArgumentException("Failed to insert row into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        if (getContext() != null){
            final int count = SaveMyDatabase.getInstance(getContext()).bus_route_dao().deleteBusRoute(ContentUris.parseId(uri));
            getContext().getContentResolver().notifyChange(uri, null);
            return count;
        }
        throw new IllegalArgumentException("Failed to delete row into " + uri);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if (getContext() != null){
            final int count = SaveMyDatabase.getInstance(getContext()).bus_route_dao().updateBusRoute(Bus_Route.fromContentValues(values));
            getContext().getContentResolver().notifyChange(uri, null);
            return count;
        }
        throw new IllegalArgumentException("Failed to update row into " + uri);
    }


}

