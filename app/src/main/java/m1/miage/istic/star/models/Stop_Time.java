package m1.miage.istic.star.models;

import android.content.ContentValues;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.sql.Time;

@Entity
public class Stop_Time {

    @PrimaryKey@NonNull
    private String stop_id;

    private String trip_id;
    private String arrival_time;
    private String departure_time;
    private String stop_sequence;

    // --- GETTER ---

    public String getTrip_id() { return trip_id; }
    public String getArrival_time() { return arrival_time; }
    public String getDeparture_time() { return departure_time; }
    public String getStop_id() { return stop_id; }
    public String getStop_sequence() { return stop_sequence; }

    // --- SETTER ---

    public void setTrip_id(String trip_id) { this.trip_id = trip_id; }
    public void setArrival_time(String arrival_time) { this.arrival_time = arrival_time; }
    public void setDeparture_time(String departure_time) { this.departure_time = departure_time; }
    public void setStop_id(String stop_id) { this.stop_id = stop_id; }
    public void setStop_sequence(String stop_sequence) { this.stop_sequence = stop_sequence; }

    // --- UTILS ---
    public static Stop_Time fromContentValues(ContentValues values) {
        final Stop_Time item = new Stop_Time();
        if (values.containsKey("trip_id")) item.setTrip_id(values.getAsString("trip_id"));
        if (values.containsKey("arrival_time")) item.setArrival_time(values.getAsString("arrival_time"));
        if (values.containsKey("departure_time")) item.setDeparture_time(values.getAsString("departure_time"));
        if (values.containsKey("stop_id")) item.setStop_id(values.getAsString("stop_id"));
        if (values.containsKey("stop_sequence")) item.setStop_sequence(values.getAsString("stop_sequence"));
        return item;
    }
}