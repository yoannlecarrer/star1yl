package m1.miage.istic.star.models;

import android.content.ContentValues;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Calendar {

    @PrimaryKey@NonNull
    private String service_id;


    private String monday;
    private String tuesday;
    private String wednesday;
    private String thursday;
    private String friday;
    private String saturday;
    private String sunday;
    private String start_date;
    private String end_date;

    // --- GETTER ---

    public String getService_id() { return service_id; }
    public String getMonday() { return monday; }
    public String getTuesday() { return tuesday; }
    public String getWednesday() { return wednesday; }
    public String getThursday() { return thursday; }
    public String getFriday() { return friday; }
    public String getSaturday() { return saturday; }
    public String getSunday() { return sunday; }
    public String getStart_date() { return start_date; }
    public String getEnd_date() { return end_date; }

    // --- SETTER ---

    public void setService_id(String service_id) { this.service_id = service_id; }
    public void setMonday(String monday) { this.monday = monday; }
    public void setTuesday(String tuesday) { this.tuesday = tuesday; }
    public void setWednesday(String wednesday) { this.wednesday = wednesday; }
    public void setThursday(String thursday) { this.thursday = thursday; }
    public void setFriday(String friday) { this.friday = friday; }
    public void setSaturday(String saturday) { this.saturday = saturday; }
    public void setSunday(String sunday) { this.sunday = sunday; }
    public void setStart_date(String start_date) { this.start_date = start_date; }
    public void setEnd_date(String end_date) { this.end_date = end_date; }

    // --- UTILS ---
    public static Calendar fromContentValues(ContentValues values) {
        final Calendar item = new Calendar();
        if (values.containsKey("service_id")) item.setService_id(values.getAsString("service_id"));
        if (values.containsKey("monday")) item.setMonday(values.getAsString("monday"));
        if (values.containsKey("tuesday")) item.setTuesday(values.getAsString("tuesday"));
        if (values.containsKey("wednesday")) item.setWednesday(values.getAsString("wednesday"));
        if (values.containsKey("thursday")) item.setThursday(values.getAsString("thursday"));
        if (values.containsKey("friday")) item.setFriday(values.getAsString("friday"));
        if (values.containsKey("saturday")) item.setSaturday(values.getAsString("saturday"));
        if (values.containsKey("sunday")) item.setSunday(values.getAsString("sunday"));
        if (values.containsKey("start_date")) item.setStart_date(values.getAsString("start_date"));
        if (values.containsKey("end_date")) item.setEnd_date(values.getAsString("end_date"));
        return item;
    }
}