package m1.miage.istic.star;


import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class MyActivity extends AppCompatActivity {
    DownloadManager downloadManagerBD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final String url = getIntent().getStringExtra("Url file");

        downloadBD(url);
        extractFilesFromZip();

}



    public void downloadBD(String url) {
        downloadManagerBD = (DownloadManager) getSystemService(getApplicationContext().DOWNLOAD_SERVICE);
        Uri uri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        //request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "myDB.zip");
        request.setTitle("Téléchargement du fichier zip ...");
        Long reference = downloadManagerBD.enqueue(request);

        do {
            downloadManagerBD.getUriForDownloadedFile(reference);

        }while(downloadManagerBD.getUriForDownloadedFile(reference) == null);


        File filepath = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/myDb.zip");
        Toast.makeText(getApplicationContext(), "Fichier de la base de données est téléchargé avec succès dans:\n" + filepath, Toast.LENGTH_LONG).show();

    }


    public void extractFilesFromZip()
    {
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS).getAbsolutePath()+"/myDb.zip");
        try
        {
            FileInputStream fin = new FileInputStream(file);
            ZipInputStream zin = new ZipInputStream(new BufferedInputStream(fin));
            ZipEntry ze = null;
            while ((ze = zin.getNextEntry()) != null)
            {
                String fileName = ze.getName();

                if(ze.isDirectory())
                {
                    dirChecker(ze.getName());
                }
                else
                {
                    FileOutputStream fout = new FileOutputStream(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_DOWNLOADS).getAbsolutePath()+"/"+fileName);

                    byte[] buffer = new byte[8192];
                    int len;
                    while ((len = zin.read(buffer)) != -1)
                    {
                        fout.write(buffer, 0, len);
                    }
                    fout.close();

                    zin.closeEntry();

                }

            }
            zin.close();
        }
        catch(Exception e)
        {
            Log.e("Decompress", "unzip", e);
        }

    }

private void dirChecker(String dir)
        {
        File f = new File(Environment.getExternalStoragePublicDirectory(
        Environment.DIRECTORY_DOWNLOADS).getAbsolutePath());
        if(!f.isDirectory())
        {
        f.mkdirs();
        }
        }





}
