package m1.miage.istic.star.models;

import android.content.ContentValues;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Bus_Route {

    @PrimaryKey@NonNull
    private String route_id;

    private String route_short_name;
    private String route_long_name;
    private String route_desc;
    private String route_type;
    private String route_color;
    private String route_text_color;


    // --- GETTER ---

    public String getRoute_short_name() { return route_short_name; }
    public String getRoute_long_name() { return route_long_name; }
    public String getRoute_desc() { return route_desc; }
    public String getRoute_type() { return route_type; }
    public String getRoute_color() { return route_color; }
    public String getRoute_text_color() { return route_text_color; }
    public String getRoute_id() { return route_id; }


    // --- SETTER ---

    public void setRoute_id(String route_id) { this.route_id = route_id; }
    public void setRoute_short_name(String route_short_name) { this.route_short_name = route_short_name; }
    public void setRoute_long_name(String route_long_name) { this.route_long_name = route_long_name; }
    public void setRoute_desc(String route_desc) { this.route_desc = route_desc; }
    public void setRoute_type(String route_type) { this.route_type = route_type; }
    public void setRoute_color(String route_color) { this.route_color = route_color; }
    public void setRoute_text_color(String route_text_color) { this.route_text_color = route_text_color; }

    // --- UTILS ---
    public static Bus_Route fromContentValues(ContentValues values) {
        final Bus_Route item = new Bus_Route();
        if (values.containsKey("route_id")) item.setRoute_id(values.getAsString("route_id"));
        if (values.containsKey("route_short_name")) item.setRoute_short_name(values.getAsString("route_short_name"));
        if (values.containsKey("route_long_name")) item.setRoute_long_name(values.getAsString("route_long_name"));
        if (values.containsKey("route_desc")) item.setRoute_desc(values.getAsString("route_desc"));
        if (values.containsKey("route_type")) item.setRoute_type(values.getAsString("route_type"));
        if (values.containsKey("route_color")) item.setRoute_color(values.getAsString("route_color"));
        if (values.containsKey("route_text_color")) item.setRoute_text_color(values.getAsString("route_text_color"));
        return item;
    }
}
