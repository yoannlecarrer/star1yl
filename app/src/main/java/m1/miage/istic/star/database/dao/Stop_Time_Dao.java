package m1.miage.istic.star.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import m1.miage.istic.star.models.Stop_Time;

@Dao
public interface Stop_Time_Dao {

    @Query("SELECT * FROM Stop_Time WHERE stop_id = :stop_id")
    LiveData<List<Stop_Time>> getStop_Time(int stop_id);

    @Insert
    long insertStop_Time(Stop_Time stop_time);

    @Update
    int updateStop_Time(Stop_Time stop_time);

    @Query("DELETE FROM Stop_Time WHERE stop_id = :stop_id")
    int deleteStop_Time(int stop_id);
}