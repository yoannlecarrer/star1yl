package m1.miage.istic.star.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import m1.miage.istic.star.models.Stop;

@Dao
public interface Stop_Dao {

    @Query("SELECT * FROM Stop WHERE stop_code = :stop_code")
    LiveData<List<Stop>> getStop(int stop_code);

    @Insert
    long insertStop(Stop stop);

    @Update
    int updateStop(Stop stop);

    @Query("DELETE FROM Stop WHERE stop_code = :stop_code")
    int deleteStop(int stop_code);
}